<?php

namespace Infinity\CMSBundle\Entity\User;

use Doctrine\ORM\Mapping as ORM;
use Infinity\CoreBundle\Entity\User as CMSUser;

/**
 * Class MainUser
 * @package Infinity\CMSBundle\Entity\User
 *
 * @ORM\MappedSuperclass()
 */
final class MainUser extends CMSUser
{

    public function __construct()
    {
        parent::__construct();
        $this->setRoles(array('ROLE_USER'));
        $this->setType('main');
    }
}