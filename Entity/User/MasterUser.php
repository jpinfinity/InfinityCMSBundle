<?php

namespace Infinity\CMSBundle\Entity\User;

use Doctrine\ORM\Mapping as ORM;
use Infinity\CoreBundle\Entity\User as InfinityUser;

/**
 * Class MasterUser
 * @package Infinity\CMSBundle\Entity\User
 *
 * @ORM\Entity()
 * @ORM\Table(name="user_master")
 */
final class MasterUser extends InfinityUser
{

    public function __construct()
    {
        parent::__construct();
        $this->setRoles(array('ROLE_MASTER'));
        $this->setType('master');
    }
}