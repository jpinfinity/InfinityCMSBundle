<?php

namespace Infinity\CMSBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Class UsersController
 * @package Infinity\CMSBundle\Controller
 */
class UsersController extends BaseController
{
    public function myAccountAction()
    {
        return $this->render('InfinityCMSBundle:Users:myaccount.html.twig');
    }

    public function listAction()
    {
        return $this->render('InfinityCMSBundle:Users:list.html.twig');
    }
}
