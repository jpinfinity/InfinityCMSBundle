<?php

namespace Infinity\CMSBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class CommonController
 * @package InfinityCMSBundle\Controller
 * @Security("has_role('ROLE_ADMIN')")
 */
class CommonController extends BaseController
{
    public function menuAction()
    {
        $menu = array(
            'infinity_cms_dashboard' => array(
                'icon'  => 'fci-dashboard',
                'title' => $this->trans('dashboard')
            ),
            'infinity_cms_contents' => array(
                'icon'  => 'fci-page-copy',
                'title' => 'Contenus'
            ),
            'infinity_cms_medias' => array(
                'icon'  => 'fci-images',
                'title' => $this->trans('media_library', array(), 'media_library')
            ),
            'infinity_cms_users' => array(
                'icon'  => 'fci-reseller-programm',
                'title' => 'Utilisateurs'
            ),
            'infinity_cms_i18n' => array(
                'icon'  => 'fci-locate',
                'title' => 'Internationalisation'
            ),
            'infinity_cms_tools' => array(
                'icon'  => 'fci-setting-tools',
                'title' => 'Outils'
            )
        );

        return $this->render('InfinityCMSBundle:Common:menu.html.twig', array('menu'=>$menu));
    }
}
