<?php

namespace Infinity\CMSBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Class I18nController
 * @package JPInfinity\CMSBundle\Controller
 * @Route("/i18n")
 */
class i18nController extends BaseController
{
    /**
     * @Route("/", name="infinitycms_i18n")
     */
    public function myAccountAction()
    {
        return $this->render('JPInfinityCMSBundle:i18n:index.html.twig');
    }
}
