<?php

namespace Infinity\CMSBundle\Controller;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseController extends Controller
{
    /** @var EntityManager */
    protected $_manager;

    /**
     * @param $id
     * @param array $parameters
     * @param null $domain
     * @param null $locale
     *
     * @return string
     */
    protected function trans($id, array $parameters = array(), $domain = null, $locale = null)
    {
        /** @ignore */
        return $this->get('translator')->trans($id, $parameters, $domain, $locale);
    }

    /**
     * @param $id
     * @param $number
     * @param array $parameters
     * @param null $domain
     * @param null $locale
     *
     * @return string
     */
    protected function transchoice($id, $number, array $parameters = array(), $domain = null, $locale = null)
    {
        /** @ignore */
        return $this->get('translator')->transchoice($id, $number, $parameters, $domain, $locale);
    }

    /**
     * @param null $name
     *
     * @return \Doctrine\Common\Persistence\ObjectManager|EntityManager|object
     */
    protected function getManager($name=null)
    {
        $this->_manager = $this->getDoctrine()->getManager($name);
        return $this->_manager;
    }

    /**
     * @param $className
     *
     * @return \Doctrine\Common\Persistence\ObjectRepository|\Doctrine\ORM\EntityRepository
     */
    protected function getRepository($className)
    {
        return $this->getManager()->getRepository($className);
    }

    /**
     * @param $path
     * @param null $packageName
     *
     * @return string
     */
    public function getAssetUrl($path, $packageName = null)
    {
        return $this->container->get('templating.helper.assets')->getUrl($path, $packageName);
    }
}
?>