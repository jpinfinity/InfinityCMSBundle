<?php

namespace Infinity\CMSBundle\Controller;

class DashboardController extends BaseController
{

    public function indexAction()
    {
        return $this->render('InfinityCMSBundle:Dashboard:index.html.twig');
    }
}
