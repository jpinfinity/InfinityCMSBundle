<?php

namespace Infinity\CMSBundle\Controller;

use Infinity\CMSBundle\Entity\MediaFolder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Form;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type as FormType;

class MediasController extends BaseController
{
    public function indexAction($id = null)
    {
        $folders = $this->getRepository('InfinityCMSBundle:MediaFolder')->getChildren($id);

        return $this->render('InfinityCMSBundle:Medias:index.html.twig', array('folders'=>$folders));
    }

    public function addFolderAction(Request $request)
    {
        $folder = new MediaFolder();

        $form = $this->_buildFolderForm($folder, $request);

        return $this->_getPostedFolderForm($form, $folder, $request, true);
    }

    public function addMediaAction(Request $request)
    {
        $folder = new MediaFolder();

        $form = $this->_buildFolderForm($folder, $request);

        return $this->_getPostedFolderForm($form, $folder, $request, true);
    }

    /**
     * @Route("/medias/folder/{id}", requirements={"id" = "\d+"}, name="infinitycms_medias.viewfolder")
     * @Route("/medias/folder/edit/{id}", requirements={"id" = "\d+"}, name="infinitycms_medias.editfolder")
     */
    public function editFolderAction($id, Request $request)
    {
        $folder = $this->getRepository('JPInfinityCMSBundle:MediaFolder')->find($id);
        $form =  $this->_buildFolderForm($folder);

        return $this->_getPostedFolderForm($form, $folder, $request, false);
    }

    private function _buildFolderForm(MediaFolder $folder)
    {
        /** @var \Symfony\Component\Form\FormBuilder $formBuilder */
        $formBuilder = $this->get('form.factory')->createBuilder(FormType\FormType::class, $folder);
        $form = $formBuilder
            ->add('name', FormType\TextType::class, array('required'=>true))
            ->add('parent_id', EntityType::class, array(
                'class' => 'InfinityCMSBundle:MediaFolder',
                'choice_label' => 'name',
                //'empty_value'=>$this->transchoice('none', 0),
                'required'=>false
            ))
            ->add('save', FormType\SubmitType::class)
            ->getForm()
        ;
        return $form;
    }

    private function _getPostedFolderForm(Form $form, MediaFolder $folder, Request $request, $isNew)
    {
        // On fait le lien Requête <-> Formulaire
        // À partir de maintenant, la variable $folder contient les valeurs entrées dans le formulaire par le visiteur
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                $this->getManager()->persist($folder);
                $this->getManager()->flush();

                $request->getSession()->getFlashBag()->add('success', $isNew == true ? 'Le dossier a bien été créé.':'Le dossier a bien été mis à jour');
                return $this->redirect($this->generateUrl('infinity_cms_medias'));
            }
            else{
                $request->getSession()->getFlashBag()->add('error', 'Une erreur est survenue.');
            }
        }

        return $this->render('InfinityCMSBundle:Medias:folderform.html.twig', array('form' => $form->createView()));
    }
}
