<?php

namespace Infinity\CMSBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ContentsController extends BaseController
{
    /**
     * @Route("/contents", name="infinitycms_contents")
     */
    public function indexAction()
    {
        return $this->render('JPInfinityCMSBundle:Contents:index.html.twig');
    }
}
