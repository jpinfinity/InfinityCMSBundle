<?php

namespace Infinity\CMSBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Class RegistrationController
 * @package Infinity\CMSBundle\Controller
 * @Route("/tools")
 */
class ToolsController extends BaseController
{
    /**
     * @Route("/", name="infinitycms_tools")
     */
    public function indexAction()
    {
        return $this->render('InfinityCMSBundle:Tools:index.html.twig');
    }
}
