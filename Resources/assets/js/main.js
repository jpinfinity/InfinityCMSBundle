var contextMenus = [];

$(document).ready(function () {
    if ($('.alert').length > 0) {
        $('.alert').show().delay(2000).fadeOut(200);
    }

    if (Object.keys(contextMenus).length > 0) {
        var i = 0;
        var contextMenu;
        $.each(contextMenus, function(selector, items){
            contextMenu = $(selector).contextify({
                hideOnMouseUp: false,
                hideOnScroll: false,
                menuId: 'context-menu-' + i,
                menuClass: 'dropdown-menu contextmenu',
                items: items
            });
            i++;
        })
    }
});