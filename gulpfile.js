var gulp = require('gulp');
var sass = require('gulp-sass');
var minifycss = require('gulp-clean-css');
var uglify = require('gulp-uglify');

gulp.task('infinity-cms-css', function() {
    gulp.src('node_modules/bootstrap/dist/**')
        .pipe(gulp.dest('Resources/public/vendor/bootstrap'));

    gulp.src('Resources/assets/sass/**/*.scss')
        .pipe(sass())
        .pipe(minifycss({compatibility: 'ie8'}))
        .pipe(gulp.dest('Resources/public/css'));
});

gulp.task('infinity-cms-js', function() {
    gulp.src('Resources/assets/js/**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('Resources/public/js'));
});

gulp.task('vendor-js', function() {
    gulp.src('node_modules/jquery/dist/**')
        .pipe(gulp.dest('Resources/public/vendor/jquery'));

    gulp.src('node_modules/jquery-contextify/dist/**')
        .pipe(gulp.dest('Resources/public/vendor/jquery-contextify'));
});

gulp.task("watch-css", ["infinity-cms-css"], function() {
    gulp.watch("./Resources/assets/sass/**/*", ["infinity-cms-css"])
});

gulp.task("watch-js", ["infinity-cms-js"], function() {
    gulp.watch("./Resources/assets/js/**/*", ["infinity-cms-js"])
});
